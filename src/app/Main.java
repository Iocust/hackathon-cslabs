package app;

import app.view.AccountView;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {

		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		AccountView view = new AccountView(null);
		view.show();
	}
}
