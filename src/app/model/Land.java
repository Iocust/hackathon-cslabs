package app.model;

import java.util.List;

public class Land {

	private Farmer prorietaire;
	private int id;
	private static int ID_INCREMENT = 0;
	private double longitude;
	private double latitude;
	private String adresse;
	private List<Product> listeProduit;

	public Land(Farmer prorietaire, double longitude, double latitude, String adresse, List<Product> listeProduit) {
		super();
		this.id = ID_INCREMENT;
		ID_INCREMENT++;
		this.prorietaire = prorietaire;
		this.longitude = longitude;
		this.latitude = latitude;
		this.adresse = adresse;
		this.listeProduit = listeProduit;
	}

	public Land(Farmer prorietaire, String adresse, List<Product> listeProduit) {
		super();
		this.prorietaire = prorietaire;
		this.adresse = adresse;
		this.listeProduit = listeProduit;
		this.id = ID_INCREMENT;
		ID_INCREMENT++;

	}

	public boolean ajouterProduit(Product p) {
		if (p == null)
			throw new IllegalArgumentException();
		return this.listeProduit.add(p);
	}

	public boolean contientCeProduit(Product p) {
		if (p == null)
			throw new IllegalArgumentException();
		return this.listeProduit.contains(p);
	}

	public Farmer getProrietaire() {
		return prorietaire;
	}

	public void setProrietaire(Farmer prorietaire) {
		this.prorietaire = prorietaire;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public List<Product> getListeProduit() {
		return listeProduit;
	}

	public void setListeProduit(List<Product> listeProduit) {
		this.listeProduit = listeProduit;
	}

	public int getId() {
		return id;
	}

}
