package app.model;

import app.model.Variety;

public class Product {
	private static int id_increment;
	private int id;
	private String nom;
	private Variety type;

	public Product(String nom, Variety type) {
		super();
		this.id = id_increment++;
		this.nom = nom;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public Variety getType() {
		return type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
