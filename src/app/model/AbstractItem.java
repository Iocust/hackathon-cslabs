package app.model;

/**
 * This is the base class of every item stored in the DB
 *
 * an item can be a Store, ShoppingList, Recipe, Product or Account
 *
 */
public abstract class AbstractItem {

	private String _name;
	private int _id;

	protected AbstractItem(int id, String name) {
		_id = id;
		_name = name;
	}

	public String getName() {
		return _name;
	}

	public int getId() {
		return _id;
	}

	public void setName(String name) {
		_name = name;
	}

	public void setId(int id) {
		_id = id;
	}

	@Override
	public String toString() {
		return _name;
	}
}
