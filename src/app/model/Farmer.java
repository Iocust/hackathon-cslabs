package app.model;

import java.util.ArrayList;
import java.util.List;

public class Farmer {
	private static int id_increment = 0;
	private int id;
	private String name;
	private String surname;
	private List<Land> lands = new ArrayList<Land>();
	private List<Tutorial> tutorials = new ArrayList<Tutorial>();
	private String mail;
	private String pwd;

	public Farmer(String name, String surname, String mail, String pwd) {
		super();
		this.id = id_increment++;
		this.name = name;
		this.surname = surname;
		this.mail = mail;
		this.pwd = pwd;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getMail() {
		return mail;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Farmer other = (Farmer) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public boolean addLand(Land l) {
		return lands.add(l);
	}

	public boolean removeLand(Land l) {
		return lands.remove(l);
	}

	public boolean addTutorial(Tutorial t) {
		return tutorials.add(t);
	}

	public boolean removeTutorial(Tutorial t) {
		return tutorials.remove(t);
	}
}
