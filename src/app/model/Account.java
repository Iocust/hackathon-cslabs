package app.model;

/**
 * This class concretizes AbstractItem. It represents an account
 */
public class Account extends AbstractItem {

	private String _password;

	public Account(int id, String name, String password) {
		super(id, name);
		_password = password;
	}

	public Account(String name, String password) {
		super(-1, name);
		_password = password;
	}

	public String getPassword() {
		return _password;
	}

	public void setPassword(String password) {
		_password = password;
	}
}
