package app.model.databaseUtils;

public class ProductListRelationUtils {
	public static final String DELETE = "DELETE FROM hasProductInShoppingList WHERE ProductId = ? AND ShoppingListId = ?";
	public static final String DELETEPRODUCT = "DELETE FROM hasProductInShoppingList WHERE ProductId = ?";
	public static final String ADD = "INSERT INTO hasProductInShoppingList (ProductId, ShoppingListId, Quantity) VALUES (?, ?, ?)";
	public static final String UPDATE = "UPDATE hasProductInShoppingList SET Quantity = ? WHERE ProductId = ? AND ShoppingListId = ?";
	public static final String GET = "SELECT Quantity FROM hasProductInShoppingList WHERE ProductId = ? AND ShoppingListId = ?";
	public static final String GETALL = "SELECT ProductId, Quantity FROM hasProductInShoppingList WHERE ShoppingListId = ?";
}
