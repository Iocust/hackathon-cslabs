package app.model.databaseUtils;

public class UserAnnonceUtils {
	
	public static final String ADD = "INSERT INTO Users_Annonces (user_id, annonce_id) VALUES (?,?)";
	public static final String UPDATE = "UPDATE Users_Annonces SET user_id = ?, annonce_id = ?";
	public static final String DELETE = "DELETE FROM Users_Annonces WHERE user_id = ? AND id_annonce = ?";
	public static final String GETBYID = "SELECT * FROM Annonces WHERE id_annonce IN (SELECT annonce_id FROM Users_Annonces WHERE user_id = ?)";
}
