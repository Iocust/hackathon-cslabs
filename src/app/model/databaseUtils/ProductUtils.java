package app.model.databaseUtils;

public class ProductUtils {
	public static final String DELETE = "DELETE FROM Product WHERE ProductId = ?";
	public static final String ADD = "INSERT INTO Product (Name) VALUES (?)";
	public static final String UPDATE = "UPDATE Product SET Name = ? WHERE ProductId = ?";
	public static final String GETBYID = "SELECT Name FROM Product WHERE ProductId = ?";
	public static final String GETBYNAME = "SELECT ProductId FROM Product WHERE Name = ?";
	public static final String GETALL = "SELECT ProductId, Name FROM Product";
}
