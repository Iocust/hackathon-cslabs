package app.model.databaseUtils;

public class RecipeUtils {
	public static final String DELETERECIPE = "DELETE FROM Recipe WHERE RecipeId = ?";
	public static final String DELETESTEPS = "DELETE FROM RecipeStep WHERE RecipeId = ?";
	public static final String ADDRECIPE = "INSERT INTO Recipe (Name, Persons, AccountId) VALUES (?, ?, ?)";
	public static final String ADDSTEP = "INSERT INTO RecipeStep (RecipeStepIndex, Instructions, RecipeId) VALUES (?, ?, ?)";
	public static final String UPDATE = "UPDATE Recipe SET Name = ?, Persons = ?, AccountId = ? WHERE RecipeId = ?";
	public static final String GETRECIPE = "SELECT Name, AccountId, Persons FROM Recipe WHERE RecipeId = ?";
	public static final String GETSTEPS = "SELECT Instructions FROM RecipeStep WHERE RecipeId = ? ORDER BY RecipeStepIndex";
	public static final String GETALL = "SELECT RecipeId FROM Recipe WHERE AccountId = ?";
}
