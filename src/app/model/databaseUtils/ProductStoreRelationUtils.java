package app.model.databaseUtils;

public class ProductStoreRelationUtils {
	public static final String DELETE = "DELETE FROM hasProductInStore WHERE ProductId = ? AND StoreId= ?";
	public static final String DELETEPRODUCT = "DELETE FROM hasProductInStore WHERE ProductId = ?";
	public static final String ADD = "INSERT INTO hasProductInStore (ProductId, StoreId, QuantityPerProduct, Price, Description) VALUES (?, ?, ?, ?, ?)";
	public static final String UPDATE = "UPDATE hasProductInStore SET QuantityPerProduct = ?, Price = ?, Description = ? WHERE ProductId = ? AND StoreId = ?";
	public static final String GET = "SELECT QuantityPerProduct, Price, Description FROM hasProductInStore WHERE ProductId = ? AND StoreId = ?";
	public static final String GETALL = "SELECT ProductId, QuantityPerProduct, Price, Description FROM hasProductInStore WHERE StoreId = ?";
	public static final String GETSTORESIDS = "SELECT DISTINCT StoreId FROM hasProductInStore WHERE ProductId = ?";
}
