package app.model.databaseUtils;

public class AccountUtils {
	public static final String DELETE = "DELETE FROM Account WHERE AccountId = ?";
	public static final String ADD = "INSERT INTO Account (Name, Password) VALUES (?, ?)";
	public static final String UPDATE = "UPDATE Account SET Name = ?, Password = ? WHERE AccountId = ?";
	public static final String GETBYNAME = "SELECT AccountId, Password FROM Account WHERE Name = ?";
	public static final String GETBYID = "SELECT Name, Password FROM Account WHERE AccountId = ?";
}
