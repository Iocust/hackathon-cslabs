package app.model.databaseUtils;

public class AnnonceUtils {
	public static final String DELETE = "DELETE FROM Annonces WHERE id_annonce = ?";
	public static final String ADD = "INSERT INTO Annonces (description, name, recompense, debut, fin, latitude, longitude, emplacement, id_creator) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	public static final String UPDATE = "UPDATE Annonces SET name = ? ,description = ?, recompense = ?, debut = ?, fin = ?, emplacement = ?, latitude = ?, longitude = ? WHERE id_annonce = ?";
	public static final String GETALL = "SELECT * FROM Annonces";
}
