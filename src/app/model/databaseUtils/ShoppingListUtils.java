package app.model.databaseUtils;

public class ShoppingListUtils {
	public static final String DELETE = "DELETE FROM ShoppingList WHERE ShoppingListId = ?";
	public static final String ADD = "INSERT INTO ShoppingList (Name, AccountId) VALUES (?, ?)";
	public static final String UPDATE = "UPDATE ShoppingList SET Name = ?, AccountId = ? WHERE ShoppingListId = ?";
	public static final String GET = "SELECT Name, AccountId FROM ShoppingList WHERE ShoppingListId = ?";
	public static final String GETALL = "SELECT ShoppingListId FROM ShoppingList WHERE AccountId = ?";

}
