package app.model.databaseUtils;

public class StoreUtils {
	public final static String DELETE = "DELETE FROM Store WHERE StoreId = ?";
	public final static String ADD = "INSERT INTO Store (Name, Address, Latitude, Longitude) VALUES (?, ?, ?, ?)";
	public static final String UPDATE = "UPDATE Store SET Name = ?, Address = ?, Latitude = ?, Longitude = ? WHERE StoreId = ?";
	public static final String GETBYID = "SELECT Name, Address, Latitude, Longitude FROM Store WHERE StoreId = ?";
	public static final String GETBYNAME = "SELECT StoreId, Address, Latitude, Longitude FROM Store WHERE Name = ?";
	public static final String GETALL = "SELECT StoreId FROM Store";
}
