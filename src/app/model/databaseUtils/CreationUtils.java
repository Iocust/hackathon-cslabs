package app.model.databaseUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class CreationUtils {

	private static String _authDb = "jdbc:sqlite:%s";
	private static String _databaseName = "AutoChefDatabase";

	/**
	 * getter
	 *
	 * @return String representing the database name
	 */
	public static String getDatabaseName() {
		return _databaseName;
	}

	public static String getAuth() {
		return String.format(_authDb, _databaseName);
	}

	/**
	 * Creates the database scripts from scriptList.txt.
	 *
	 * @param conn Connection - Connection with the database.
	 * @throws SQLException
	 * @throws IOException
	 */
	public static void createDatabase(Connection conn) throws SQLException, IOException {

		java.io.InputStream in = InputStream.class.getResourceAsStream("/scriptList.txt");

		BufferedReader reader = new BufferedReader(new java.io.InputStreamReader(in));
		String line = reader.readLine();
		while (line != null) {
			java.io.InputStream scriptIn = InputStream.class.getResourceAsStream(line);
			BufferedReader scriptReader = new BufferedReader(new java.io.InputStreamReader(scriptIn));
			executeScript(conn, scriptReader);

			line = reader.readLine();
		}
		reader.close();

	}

	/**
	 * execute scripts given by parameter path.
	 *
	 * @param conn   Connection - Connection with the database.
	 * @param reader reader opened on the scripts file
	 * @throws SQLException
	 * @throws IOException
	 */
	private static void executeScript(Connection conn, BufferedReader reader) throws SQLException, IOException {
		Statement statement = conn.createStatement();

		String line = reader.readLine();

		while (line != null) {
			String nextLine = "";
			while (nextLine != null && (line.length() == 0 || line.charAt(line.length() - 1) != ';')) {
				line = String.format("%s%s", line, nextLine);
				nextLine = reader.readLine();
			}
			if (line.length() != 0 && line.charAt(line.length() - 1) == ';')
				statement.execute(line);
			line = reader.readLine();
		}
		reader.close();
	}

	public static void switchTest() {
		_databaseName = "test_AutoChefDatabase";
	}
}
