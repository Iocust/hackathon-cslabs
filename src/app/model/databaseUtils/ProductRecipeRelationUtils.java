package app.model.databaseUtils;

public class ProductRecipeRelationUtils {
	public static final String DELETE = "DELETE FROM hasProductInRecipe WHERE ProductId = ? AND RecipeId = ?";
	public static final String DELETEPRODUCT = "DELETE FROM hasProductInRecipe WHERE ProductId = ?";
	public static final String ADD = "INSERT INTO hasProductInRecipe (ProductId, RecipeId, Quantity) VALUES (?, ?, ?)";
	public static final String UPDATE = "UPDATE hasProductInRecipe SET Quantity = ? WHERE ProductId = ? AND RecipeId = ?";
	public static final String GET = "SELECT Quantity FROM hasProductInRecipe WHERE ProductId = ? AND RecipeId = ?";
	public static final String GETALL = "SELECT ProductId, Quantity FROM hasProductInRecipe WHERE RecipeId = ?";
}
