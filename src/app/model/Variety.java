package app.model;

public enum Variety {
	VEGETABLE, FRUIT, CEREAL;
}
