package app.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Annonce extends AbstractItem {

	private String description;
	private String creator;
	private String recompense;
	private LocalDate debut;
	private LocalDate fin;
	private String emplacement;
	private double latitude;
	private double longitude;

	public Annonce(int id, String description, String name, String recompense, LocalDate debut, LocalDate fin,
			double latitude, double longitude, String emplacement, String createur) {
		super(id, name);
		this.description = description;
		this.creator = createur;
		this.recompense = recompense;
		this.debut = debut;
		this.fin = fin;
		this.emplacement = emplacement;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public Annonce(String name, String description, String createur, String recompense, LocalDate debut, LocalDate fin,
			String emplacement, double latitude, double longitude) {
		super(-1, name);
		this.description = description;
		this.creator = createur;
		this.recompense = recompense;
		this.debut = debut;
		this.fin = fin;
		this.emplacement = emplacement;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public Annonce() {
		super(-1, "Generic");
		this.description = "Generic Description";
		this.creator = "1";
		this.recompense = "Generic Recompense";
		this.debut = LocalDate.now();
		this.fin = LocalDate.now();
		this.emplacement = "Generembloux";
		this.latitude = (double) 50;
		this.longitude = (double) 50;
	}

	public LocalDate getDebut() {
		return debut;
	}

	public void setDebut(LocalDate debut) {
		this.debut = debut;
	}

	public LocalDate getFin() {
		return fin;
	}

	public void setFin(LocalDate fin) {
		this.fin = fin;
	}

	public String getEmplacement() {
		return emplacement;
	}

	public void setEmplacement(String emplacement) {
		this.emplacement = emplacement;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPosteur() {
		return creator;
	}

	public String getRecompense() {
		return recompense;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public String toString() {
		if (this.debut.equals(this.fin))
			return "Annonce publiee par " + creator + " pour le " + debut + ".\n Contenu : " + description
					+ "\n Le lieu de rendez-vous se situe a " + emplacement + " et vous recevrez " + recompense + ".";
		return "Annonce publiee par " + creator + " entre le " + debut + " et le " + fin + ".\n Contenu : "
				+ description + "\n Le lieu de rendez-vous se situe a " + emplacement + " et vous recevrez "
				+ recompense + ".";
	}

}
