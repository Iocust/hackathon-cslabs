package app.model;

import java.time.LocalDateTime;

public class Tutorial {
	private static int id_increment = 0;
	private int id;
	private LocalDateTime datePost;
	private String content;
	private Farmer creator;
	private Product theme;
	private String title;
	private String description;

	public Tutorial(LocalDateTime datePost, String tuto, Farmer creator, Product theme, String title,
			String description) {
		super();
		this.id = id_increment++;
		this.datePost = datePost;
		this.content = tuto;
		this.creator = creator;
		this.theme = theme;
		this.title = title;
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Product getTheme() {
		return theme;
	}

	public void setTheme(Product theme) {
		this.theme = theme;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getDatePost() {
		return datePost;
	}

	public Farmer getCreator() {
		return creator;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tutorial other = (Tutorial) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
