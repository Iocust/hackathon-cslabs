package app.model;

public class UserAnnonce extends AbstractItem {
	
	private int userId;
	private int annonceId;
	
	public UserAnnonce(int userId, int annonceId) {
		super(0, null);
		this.userId = userId;
		this.annonceId = annonceId;
	}

	public int getUserId() {
		return userId;
	}

	public int getAnnonceId() {
		return annonceId;
	}
	
	
	
}
