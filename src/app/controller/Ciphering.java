package app.controller;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * 
 * the Ciphering class is generating a key, creating and initialising a cipher
 * object, encrypting a file, and then decrypting it. We use the Advanced
 * Encryption Standard (AES).
 * 
 * @author Pieer Lahdo
 * @version 1.0
 */

public class Ciphering {

	private static Logger _logger = Logger.getLogger("Ciphering");
	private static Ciphering _instance = null;
	private SecretKey _aesKey;
	private String _SecretKeyString = "ggbdGRqzvmfk0E8J20Zm4A==";

	public static Ciphering getInstance() {
		if (_instance == null) {
			_instance = new Ciphering();
		}
		return _instance;
	}

	public Ciphering() {
		generateKey();
	}

	/**
	 * Generating an AES Key To create an AES key, we have to instantiate a
	 * KeyGenerator for AES.
	 */
	private void generateKey() {
		if (_SecretKeyString != null) {
			byte[] decodedKey = Base64.getDecoder().decode(_SecretKeyString);
			_aesKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
		} else {
			KeyGenerator keygen = null;
			try {
				keygen = KeyGenerator.getInstance("AES");
			} catch (NoSuchAlgorithmException e) {
				_logger.log(Level.SEVERE, e.getMessage());
			}
			_aesKey = keygen.generateKey();
			// transform aeskey to string
			String encodedKey = Base64.getEncoder().encodeToString(_aesKey.getEncoded());
			System.out.println(encodedKey);
		}

	}

	/**
	 * Encrypts the given clearText using AES cipher
	 * 
	 * @param clearText String text to be encrypted
	 * @return Byte array cipherTextByte encrypted text
	 */
	public byte[] encrypt(String clearText) {

		byte[] clearTextByte;
		byte[] cipherTextByte = null;
		Cipher aesCipher = null;
		// Create the cipher
		try {

			aesCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");

		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			_logger.log(Level.SEVERE, e.getMessage());
		}

		// Initialize the cipher for encryption
		try {

			aesCipher.init(Cipher.ENCRYPT_MODE, _aesKey);

		} catch (InvalidKeyException e) {
			_logger.log(Level.SEVERE, e.getMessage());
		}

		clearTextByte = clearText.getBytes();
		try {

			cipherTextByte = aesCipher.doFinal(clearTextByte);

		} catch (IllegalBlockSizeException | BadPaddingException e) {
			_logger.log(Level.SEVERE, e.getMessage());
		}

		return cipherTextByte;

	}

	/**
	 * Decrypts cipherTextByte using AES cipher
	 * 
	 * @param cipherTextByte Byte array Encrypted text to be decrypted
	 * @return String clearTextString decrypted text
	 */
	public String decrypt(byte[] cipherTextByte) {

		byte[] clearTextByte = null;
		String clearTextString = null;
		Cipher aesCipher = null;
		// Create the cipher
		try {

			aesCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");

		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			_logger.log(Level.SEVERE, e.getMessage());
		}

		// Initialize the cipher for encryption
		try {

			aesCipher.init(Cipher.DECRYPT_MODE, _aesKey);

		} catch (InvalidKeyException e) {
			_logger.log(Level.SEVERE, e.getMessage());
		}

		try {

			clearTextByte = aesCipher.doFinal(cipherTextByte);

		} catch (IllegalBlockSizeException | BadPaddingException e) {
			_logger.log(Level.SEVERE, e.getMessage());
		}

		clearTextString = new String(clearTextByte);
		return clearTextString;

	}

}
