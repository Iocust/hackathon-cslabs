package app.controller;

import app.controller.facade.AccountFacade;
import app.view.AccountView;
import app.view.ConsulterAnnoncesView;
import app.view.CreationAnnonceView;
import app.view.MenuView;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Class used for the functions used by the MenuView described in the MenuView
 * FXML file. Communicates with its controller to realize the user's requests.
 * Is instantiated by the FXML file MenuView and @FXML fields are used for node
 * and method injection. The nodes and methods are referenced by the FXML file.
 * For more information, see doc on javaFX and FXML. NOTE: This class, while
 * called controller, is in fact a JavaFX controller, which is the "code"
 * referenced by the FXML file. It does not actually fulfill the role of an
 * "MVC-type" controller.
 */
public class MenuViewHandler extends AbstractViewHandler<MenuView> {

	@FXML
	public Button btnShoppingList;
	@FXML
	public Button btnRecipe;
	@FXML
	public Button btnLogout;
	@FXML
	public Button btnExit;
	@FXML
	public Button btnMap;
	@FXML
	public Button btnDesc;
	@FXML
	public Label titleLabel;
	@FXML
	public GridPane centralPain;
	@FXML
	public GridPane buttonGrid;
	@FXML
	public Button btnBrowse;
	@FXML
	public MenuItem menuBrowse;
	@FXML
	public MenuItem menuCreate;
	@FXML
	public MenuItem menuLogin;
	@FXML
	public MenuItem menuList;

	private boolean isProducteur;

	/**
	 * Handles user click on the shopping list button. Initiates display of shopping
	 * list window.
	 */
	@FXML
	public void handlebtnShoppingList() {
		Stage stg = (Stage) btnBrowse.getScene().getWindow();
		stg.close();
		ConsulterAnnoncesView annoncesView = new ConsulterAnnoncesView(null);
		annoncesView.show();
		annoncesView.getHandler().init();
	}

	@FXML
	public void handlebtnMap() {
		CreationAnnonceView crv = new CreationAnnonceView(null);
		crv.show();
		crv.getHandler().init();
	}

	public void activateProducteur() {
		isProducteur = true;
	}

	public void init() {
		if (isProducteur) {
			btnMap.setDisable(false);
		}
	}

	/**
	 * Handles button click on new recipe button. Initiates display of recipe
	 * creation pop-up.
	 */
	@FXML
	public void handlebtnRecipe() {
		// CreateRecipeView crv = new CreateRecipeView(null);
		// crv.show();
	}

	@FXML
	public void handlebtnDesc() {
		// clearGrids();
		// new DescriptionView(centralPain, buttonGrid);
		// titleLabel.setText("Browse Store Products");
	}

	/**
	 * Handles button click on browse recipes button. Initiates display of recipe
	 * browsing view.
	 */
	@FXML
	public void handlebtnBrowse() {
		// clearGrids();
		// BrowseRecipeListView brlv = new BrowseRecipeListView(centralPain);
		// titleLabel.setText("Browse Recipes");
		// brlv.show();
	}

	public void clearGrids() {
		centralPain.getChildren().clear();
		buttonGrid.getChildren().clear();
	}

	/**
	 * Handles user click on logout button. Initiates display of initial login
	 * window.
	 */
	@FXML
	public void handlebtnLogout() {
		AccountFacade facade = new AccountFacade();
		facade.logout();
		transitionToLogin();
	}

	/**
	 * Transitions to login screen when logging out.
	 */
	private void transitionToLogin() {
		AccountView accView = new AccountView(null);
		accView.show();
		Stage stg = (Stage) btnLogout.getScene().getWindow();
		stg.close();
	}

	/**
	 * Handles user click on exit button. Exits the application.
	 */
	@FXML
	public void handlebtnExit() {
		exitApp();
	}

	/**
	 * Exits the application.
	 */
	public void exitApp() {
		Platform.exit();
	}
}
