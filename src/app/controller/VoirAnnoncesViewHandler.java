package app.controller;

import app.model.Annonce;
import app.view.VoirAnnoncesView;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

public class VoirAnnoncesViewHandler extends AbstractViewHandler<VoirAnnoncesView> {
	@FXML
	Label labelTitre;
	@FXML
	Button btnPost;
	@FXML
	DatePicker dateAnnonce;
	@FXML
	TextArea recompField;
	@FXML
	TextArea descriptionField;
	@FXML
	Label labelEmplacement;

	@FXML
	public void handleQuit() {
		Stage stg = (Stage) btnPost.getScene().getWindow();
		stg.close();
	}

	public void setupAnnonce(Annonce ann) {
		labelTitre.setText(ann.getName());
		//dateAnnonce.setValue(arg0);
		recompField.setText(ann.getRecompense());
		descriptionField.setText(ann.getDescription());
		labelEmplacement.setText(ann.getEmplacement());
		//dateAnnonce.setValue(arg0);
	}

}
