package app.controller;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import app.controller.dao.AnnonceDAO;
import app.controller.facade.AccountFacade;
import app.model.Annonce;
import app.view.CreationAnnonceView;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class CreationAnnonceViewHandler extends AbstractViewHandler<CreationAnnonceView> {

	@FXML
	public ChoiceBox<String> choiceLocalite;
	@FXML
	public TextField titleField;
	@FXML
	public Button btnPost;
	@FXML
	public DatePicker dateAnnonce;
	@FXML
	public TextArea descriptionField;
	@FXML
	public TextArea recompField;

	public void init() {
		List<String> localiteList = Arrays.asList("Namur", "Ottignies", "Perpete-Les-Bains");
		choiceLocalite.getItems().addAll(localiteList);
	}

	@FXML
	public void handlePost() {
		AnnonceDAO dao = new AnnonceDAO();
		AccountFacade facade = new AccountFacade();
		dao.add(new Annonce(14, descriptionField.getText(), titleField.getText(), recompField.getText(),
				LocalDate.now(), LocalDate.now(), (double) 50, (double) 50, choiceLocalite.getValue(),
				facade.getAccountName()));

		Stage stg = (Stage) btnPost.getScene().getWindow();
		stg.close();
	}

}
