package app.controller;

import app.view.AbstractView;

public abstract class AbstractViewHandler<T extends AbstractView<?>> {

	private T _view;

	public void setView(T view) {
		_view = view;
	}

	public T getView() {
		return _view;
	}

}
