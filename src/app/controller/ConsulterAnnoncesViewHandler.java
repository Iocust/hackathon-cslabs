package app.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import app.controller.dao.AnnonceDAO;
import app.controller.dao.UserAnnonceDAO;
import app.controller.facade.AccountFacade;
import app.model.Annonce;
import app.model.UserAnnonce;
import app.view.AccountView;
import app.view.ConsulterAnnoncesView;
import app.view.CreationAnnonceView;
import app.view.VoirAnnoncesView;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ConsulterAnnoncesViewHandler extends AbstractViewHandler<ConsulterAnnoncesView> {
	@FXML
	public Button btnShoppingList;
	@FXML
	public Button btnRecipe;
	@FXML
	public Button btnLogout;
	@FXML
	public Button btnExit;
	@FXML
	public Button btnMap;
	@FXML
	public Button btnDesc;
	@FXML
	public Label titleLabel;
	@FXML
	public GridPane centralPain;
	@FXML
	public GridPane buttonGrid;
	@FXML
	public Button btnBrowse;
	@FXML
	public ChoiceBox<String> choiceLocalite;
	@FXML
	public Button btnConsulter;
	@FXML
	public Button btnPostuler;

	@FXML
	public ListView<Annonce> AnnoncesList;
	public ObservableList<Annonce> annonces;

	private boolean isProducteur;

	/**
	 * Handles user click on the shopping list button. Initiates display of shopping
	 * list window.
	 */
	@FXML
	public void handlebtnShoppingList() {
		Stage stg = (Stage) btnBrowse.getScene().getWindow();
		stg.close();
		ConsulterAnnoncesView annoncesView = new ConsulterAnnoncesView(null);
		annoncesView.show();
		annoncesView.getHandler().init();

	}

	public void activateProducteur() {
		isProducteur = true;
	}

	public void init() {
		List<String> localiteList = Arrays.asList("Namur", "Ottignies", "Perpete-Les-Bains");
		choiceLocalite.getItems().addAll(localiteList);
		if (isProducteur) {
			btnMap.setDisable(false);
		}
		AnnonceDAO dao = new AnnonceDAO();
		ArrayList<Annonce> list = dao.getAnnonces();
		annonces = FXCollections.observableArrayList(list);
		AnnoncesList.setItems(annonces);
	}

	/**
	 * Handles button click on new recipe button. Initiates display of recipe
	 * creation pop-up.
	 */
	@FXML
	public void handlebtnRecipe() {
		// CreateRecipeView crv = new CreateRecipeView(null);
		// crv.show();
	}

	@FXML
	public void handlebtnMap() {
		CreationAnnonceView crv = new CreationAnnonceView(null);
		crv.show();
	}

	@FXML
	public void handlebtnDesc() {
		// clearGrids();
		// new DescriptionView(centralPain, buttonGrid);
		// titleLabel.setText("Browse Store Products");
	}

	/**
	 * Handles button click on browse recipes button. Initiates display of recipe
	 * browsing view.
	 */
	@FXML
	public void handlebtnBrowse() {
		// clearGrids();
		// BrowseRecipeListView brlv = new BrowseRecipeListView(centralPain);
		// titleLabel.setText("Browse Recipes");
		// brlv.show();

	}

	public void clearGrids() {
		centralPain.getChildren().clear();
		buttonGrid.getChildren().clear();
	}

	/**
	 * Handles user click on logout button. Initiates display of initial login
	 * window.
	 */
	@FXML
	public void handlebtnLogout() {
		AccountFacade facade = new AccountFacade();
		facade.logout();
		transitionToLogin();
	}

	/**
	 * Handles button click on browse recipes button. Initiates display of recipe
	 * browsing view.
	 */
	@FXML
	public void handlebtnConsulter() {
		VoirAnnoncesView voir = new VoirAnnoncesView(null);
		voir.show();
		voir.getHandler().setupAnnonce(AnnoncesList.getSelectionModel().getSelectedItem());
	}

	/**
	 * Handles button click on browse recipes button. Initiates display of recipe
	 * browsing view.
	 */
	@FXML
	public void handlebtnPostuler() {
		UserAnnonceDAO dao = new UserAnnonceDAO();
		AccountFacade facade = new AccountFacade();
		dao.add(new UserAnnonce(facade.getAccountId(), AnnoncesList.getSelectionModel().getSelectedItem().getId()));
	}

	/**
	 * Transitions to login screen when logging out.
	 */
	private void transitionToLogin() {
		AccountView accView = new AccountView(null);
		accView.show();
		Stage stg = (Stage) btnLogout.getScene().getWindow();
		stg.close();
	}

	/**
	 * Handles user click on exit button. Exits the application.
	 */
	@FXML
	public void handlebtnExit() {
		exitApp();
	}

	/**
	 * Exits the application.
	 */
	public void exitApp() {
		Platform.exit();
	}

}
