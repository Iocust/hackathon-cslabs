package app.controller;

import org.controlsfx.control.textfield.CustomPasswordField;
import org.controlsfx.control.textfield.CustomTextField;

import app.controller.facade.AccountFacade;
import app.view.AccountView;
import app.view.MenuView;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * Class used for the functions used by the Account view described in the
 * LoginGUI FXML file. Communicates with its controller to realize the user's
 * requests. Is instantiated by the FXML file LoginGUI and @FXML fields are used
 * for node and method injection. The nodes and methods are referenced by the
 * FXML file. For more information, see doc on javaFX and FXML. NOTE: This
 * class, while called controller, is in fact a JavaFX controller, which is the
 * "code" referenced by the FXML file. It does not actually fulfill the role of
 * an "MVC-type" controller.
 */

public class AccountViewHandler extends AbstractViewHandler<AccountView> {

	private AccountFacade _facade;

	@FXML
	public CustomTextField username;
	@FXML
	public CustomPasswordField password;
	@FXML
	public Button logButton;
	@FXML
	public Button regButton;
	@FXML
	public CheckBox cbTerms;
	@FXML
	public Button quitButton;
	@FXML
	public ToggleButton toggleProducteur;

	/**
	 * Initializes class fields and instantiates class.
	 */
	public AccountViewHandler() {
		_facade = new AccountFacade();
	}

	/**
	 * Used to exit the application.
	 */
	public void exitApp() {
		Platform.exit();
	}

	/**
	 * Transitions to main menu view.
	 */
	private void transitionToMenu() {
		MenuView menu = new MenuView(null);
		menu.show();
		System.out.println(toggleProducteur.isSelected());
		if (toggleProducteur.isSelected()) {
			menu.getHandler().activateProducteur();
			menu.getHandler().init();
		}
		Stage stg = (Stage) quitButton.getScene().getWindow();
		stg.close();
	}

	/**
	 * Handles user click on the login button. Takes the information from the
	 * visible username and password fields. Sends a login request to the
	 * controller.
	 */
	@FXML
	public void handleLoginButton() {
		String user = username.getText();
		String pass = password.getText();
		if (_facade.authenticate(user, pass)) {
			transitionToMenu();
		} else {
			System.out.println(getView());
			getView().displayError("Failed to login", "Are you sure you've entered the right username and password?");
		}
	}

	@FXML
	public void handleEnterPressed(KeyEvent keyEvent) {
		if (keyEvent.getCode() == KeyCode.ENTER)
			handleLoginButton();
	}

	/**
	 * Handles user click on the exit button.
	 */
	@FXML
	public void handleQuitButton() {
		exitApp();
	}

	/**
	 * Handles user click on the register button. Takes the information from the
	 * visible username and password fields. Sends a register request to the
	 * controller, followed by a login request with the same data.
	 */
	@FXML
	public void handleRegButton() {

		String user = username.getText();
		String pass = password.getText();
		if (_facade.createAccount(user, pass)) {
			handleLoginButton();
		} else {
			getView().displayError("Failed to register", "This username is already taken. Please choose another one.");
		}

	}

	/**
	 * Handles user click on exit button. Exits the application.
	 */
	@FXML
	public void handlebtnExit() {
		exitApp();
	}

	/**
	 * Handle for the Browse Recipe help section.
	 */
	@FXML
	public void handlemenuBrowse() {
		handleHelp("/help/Browse_Recipe.txt", "Browse Recipe");
	}

	/**
	 * Handle for the Create Recipe help section.
	 */
	@FXML
	public void handlemenuCreate() {
		handleHelp("/help/Create_Recipe.txt", "Create Recipe");
	}

	/**
	 * Handle for the Login help section.
	 */
	@FXML
	public void handlemenuLogin() {
		handleHelp("/help/Login.txt", "Login");
	}

	/**
	 * Handle for the Shopping List help section.
	 */
	@FXML
	public void handlemenuList() {
		handleHelp("/help/Shopping_List.txt", "Shopping List");
	}

	/**
	 * Handle Map help section.
	 */
	@FXML
	public void handlemenuMap() {
		handleHelp("/help/Map.txt", "Map");

	}

	/**
	 * Handle populating the Help Window, called from different functions to use
	 * different files.
	 * 
	 * @param fileName
	 * @param title
	 */
	public void handleHelp(String fileName, String title) {
		// HelpWindowView helpWindow = new HelpWindowView(null);
		// HelpWindowViewHandler ctrl = helpWindow.getHandler();
		// ctrl.populateTextArea(fileName);
		// ctrl.setTitleLabel(title);
		// helpWindow.show();
	}

	/**
	 * Launches the view using the primary stage given to it as a parameter. This
	 * displays and initializes the view.
	 * 
	 * @param primarystage
	 */
	public void start(Stage primarystage) {

	}
}
