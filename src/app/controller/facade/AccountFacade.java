package app.controller.facade;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

import app.controller.Ciphering;
import app.controller.dao.AccountDAO;
import app.model.Account;

/**
 * This class concretizes AbstractFacade. This must be the first facade
 * instantiated in the program. Through this facade, the user can log in, which
 * sets the account to a not null value. If account is a null value, the user is
 * considered as not connected and other facades will throw an error on
 * instantiation
 */
public class AccountFacade extends AbstractFacade<Account> {

	private static Logger _logger = Logger.getLogger("AccountFacade");

	public AccountFacade() {
		super(new AccountDAO());
	}

	public AccountFacade(AccountDAO dao) {
		super(dao);
	}
	
	public String getAccountName() {
		return getAccount().getName();
	}

	/**
	 * Requests an authentication from the database using the given username and
	 * password.
	 *
	 * @param username - The user's username
	 * @param password - The user's password
	 * @return A boolean indicating the operation's status
	 * @throws IOException
	 */
	public boolean authenticate(String username, String password) {
		byte[] encryptedPassword;
		Ciphering cipher = Ciphering.getInstance();
		encryptedPassword = cipher.encrypt(password);
		String encryptedPasswordStr = null;
		try {
			encryptedPasswordStr = new String(encryptedPassword, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			_logger.log(Level.SEVERE, e.getMessage());
		}
		Account account = ((AccountDAO) _itemDAO).get(username);
		if (account != null && account.getPassword().equals(encryptedPasswordStr))
			setAccount(account);
		return isLoggedIn();
	}

	/**
	 * Used to log the user out.
	 *
	 * @return A boolean indicating the operation's status
	 */
	public void logout() {
		setAccount(null);
	}

	/**
	 * Requests the database to create an account given a username and a password.
	 *
	 * @param username - The desired username
	 * @param password - The desired password
	 * @return A boolean indicating the operation's status
	 * @throws IOException
	 */
	public boolean createAccount(String username, String password) {
		byte[] encryptedPassword;
		Ciphering cipher = Ciphering.getInstance();
		encryptedPassword = cipher.encrypt(password);
		String encryptedPasswordStr = null;
		try {
			encryptedPasswordStr = new String(encryptedPassword, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			_logger.log(Level.SEVERE, e.getMessage());
		}
		Account account = new Account(username, encryptedPasswordStr);
		if (((AccountDAO) _itemDAO).get(username) == null && add(account))
			setAccount(account);
		return isLoggedIn();
	}
}
