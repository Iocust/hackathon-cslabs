package app.controller.facade;

import app.controller.dao.AbstractItemDAO;
import app.model.AbstractItem;
import app.model.Account;

/**
 * This class is the base of all facades used to hide the data access objects.
 * It serves as an interface for the controller to manipulate the model classes
 * more easily.
 *
 * @param <T> the model object being manipulated, it is a concretization of
 *        AbstractItem
 */
public abstract class AbstractFacade<T extends AbstractItem> {

	// account is used many times in many queries to the DB, it is stored here
	private static Account _account = null;
	protected AbstractItemDAO<T> _itemDAO;

	protected AbstractFacade(AbstractItemDAO<T> dao) {
		_itemDAO = dao;
	}

	protected static void setAccount(Account account) {
		_account = account;
	}

	protected static Account getAccount() {
		return _account;
	}

	public static int getAccountId() {
		return _account.getId();
	}

	public static boolean isLoggedIn() {
		return _account != null;
	}

	// the three next methods are just delegation to the DAO object
	protected boolean add(T item) {
		return _itemDAO.add(item);
	}

	protected boolean update(T item) {
		return _itemDAO.update(item);
	}

	protected boolean delete(T item) {
		return _itemDAO.delete(item);
	}

}
