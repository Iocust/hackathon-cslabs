package app.controller.dao;

import static app.model.databaseUtils.CreationUtils.createDatabase;
import static app.model.databaseUtils.CreationUtils.getAuth;
import static app.model.databaseUtils.CreationUtils.getDatabaseName;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import app.model.databaseUtils.CreationUtils;

/**
 *
 * Data access object (DAO) the class provides an abstract interface to some
 * type of database.
 *
 * Basic methods get, add, update and delete are present (and abstract)
 *
 * protected method to interact with the DB are provided
 *
 * @param <T>
 *            is the model Java Object containing the data stored in the DB
 */
abstract class AbstractDAO<T> {

	private static Connection _connection = null;
	private static Logger _logger = Logger.getLogger("AbstractDAO");

	/**
	 * get a valid connection used in interactions with DB
	 * 
	 * @return Connection a valid connection
	 */
	protected static Connection getConnection() {
		boolean needsCreation = !new File(getDatabaseName()).exists();
		try {
			String authDb = getAuth();
			_connection = DriverManager.getConnection(authDb);
		} catch (SQLException e) {
			_logger.log(Level.SEVERE, e.getMessage());
		}
		if (needsCreation)
			try {
				createDatabase(_connection);
			} catch (IOException | SQLException e) {
				_logger.log(Level.SEVERE, e.getMessage());
			}
		return _connection;
	}

	/**
	 * method used to close connection to avoid locking the database
	 */
	protected static void closeConnection() {
		try {
			if (_connection != null && !_connection.isClosed())
				_connection.close();
		} catch (SQLException e) {
			_logger.log(Level.SEVERE, e.getMessage());
		}
	}

	/**
	 * method used to switch to test database. Used during development
	 */
	public static void switchTest() {
		CreationUtils.switchTest();
		getConnection(); // force creation of DB
		closeConnection();
	}

	/**
	 * Set the values on the statement and prepare statement.
	 * 
	 * @param statement
	 *            String - SQL query
	 * @param values
	 *            String - values to be set in PreparedStatement
	 * @return Query ready
	 */
	protected static PreparedStatement add(String statement, String... values) {
		Connection connection = getConnection();
		PreparedStatement prepStat;
		try {
			prepStat = connection.prepareStatement(statement);
			for (int i = 0; i < values.length; ++i) {
				prepStat.setString(i + 1, values[i]);
			}
			prepStat.executeUpdate();
			return prepStat;
		} catch (SQLException e) {
			_logger.log(Level.WARNING, e.getMessage());
			return null;
		}
	}

	/**
	 * Updates the database.
	 * 
	 * @param statement
	 *            String - SQL query.
	 * @param values
	 *            String - values to be set in PreparedStatement.
	 * @return boolean to test if the updates were donne correctly.
	 */
	protected static boolean update(String statement, String... values) {
		Connection connection = getConnection();
		try {
			PreparedStatement prepStat = connection.prepareStatement(statement);
			for (int i = 0; i < values.length; ++i)
				prepStat.setString(i + 1, values[i]);
			prepStat.executeUpdate();
			closeConnection();
			return true;
		} catch (SQLException e) {
			_logger.log(Level.WARNING, e.getMessage());
			closeConnection();
			return false;
		}
	}

	/**
	 * Delete values from the database.
	 * 
	 * @param statement
	 *            String - SQL query.
	 * @param ids
	 *            int - List of ID to be deleted from the database.
	 * @return boolean to test if suppression were donne correctly.
	 */
	protected static boolean delete(String statement, int... ids) {
		Connection connection = getConnection();
		try {
			PreparedStatement prepStat = connection.prepareStatement(statement);
			for (int i = 0; i < ids.length; ++i)
				prepStat.setInt(i + 1, ids[i]);
			prepStat.executeUpdate();
			closeConnection();
			return true;
		} catch (SQLException e) {
			_logger.log(Level.WARNING, e.getMessage());
			closeConnection();
			return false;
		}
	}

	/**
	 * addStep an object to database
	 * 
	 * @param object
	 *            the object added in DB
	 * @return boolean whether the insert has been successful or not
	 */
	public abstract boolean add(T object);

	/**
	 * delete an object in database
	 * 
	 * @param object
	 *            the object deleted in DB
	 * @return boolean whether the delete has been successful or not
	 */
	public abstract boolean delete(T object);

	/**
	 * update an object in database
	 * 
	 * @param object
	 *            the object updated in DB
	 * @return boolean whether the update has been successful or not
	 */
	public abstract boolean update(T object);
}
