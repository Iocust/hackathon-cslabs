package app.controller.dao;


import static app.model.databaseUtils.UserAnnonceUtils.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import app.model.UserAnnonce;
import app.model.Annonce;

public class UserAnnonceDAO extends AbstractItemDAO<UserAnnonce> {
	
	private static Logger _logger = Logger.getLogger("UserAnnonceDAO");

	@Override
	public boolean add(UserAnnonce object) {
		// TODO Auto-generated method stub
		return add(object, ADD, Integer.toString(object.getAnnonceId()), Integer.toString(object.getUserId()));
	}

	@Override
	public boolean delete(UserAnnonce object) {
		return delete(DELETE, object.getUserId(), object.getAnnonceId());
	}

	@Override
	public boolean update(UserAnnonce object) {
		return update(UPDATE, Integer.toString(object.getUserId()), Integer.toString(object.getAnnonceId()));
	}

	@Override
	public UserAnnonce get(int id) {
		// TODO Auto-generated method stu
		return null;
	}
	
	public ArrayList<Annonce> getId(int id){
		ResultSet resultSet = get(GETBYID);
		if (resultSet != null)
			try {
				ArrayList<Annonce> res = new ArrayList<Annonce>();
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				do {
					res.add(new Annonce(Integer.valueOf(resultSet.getString(0)), resultSet.getString(1),
							resultSet.getString(2), resultSet.getString(3), LocalDate.parse(resultSet.getString(4), formatter),
							LocalDate.parse(resultSet.getString(5), formatter), Double.valueOf(resultSet.getString(6)),
							Double.valueOf(resultSet.getString(7)), resultSet.getString(8),
							(resultSet.getString(9))));
				} while (resultSet.next());
				closeConnection();
				return res;
			} catch (SQLException e) {
				_logger.log(Level.WARNING, e.getMessage());
			}
		closeConnection();
		return null;
	}
	
}
