package app.controller.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import app.model.AbstractItem;

/**
 * This DAO is one of the two specializations of AbstractDAO. It focuses on
 * items rather than relations.
 *
 * Items can be Products, Account, Stores, Recipes and ShoppingLists
 *
 * @param <T> is one of the items enumerated above
 */
public abstract class AbstractItemDAO<T extends AbstractItem> extends AbstractDAO<T> {

	private static Logger _logger = Logger.getLogger("AbstractItemDAO");

	/**
	 * used when adding a new item in database. It sets the id of the model item to
	 * the one used in DB
	 * 
	 * @param preparedStatement prepared statement used while interacting with the
	 *                          DB
	 * @param item              the item added in DB
	 * @return boolean whether the update has been successful or not
	 */
	private static boolean setGeneratedId(PreparedStatement preparedStatement, AbstractItem item) {
		ResultSet generatedKeys;
		try {
			generatedKeys = preparedStatement.getGeneratedKeys();

		} catch (SQLException e) {
			_logger.log(Level.WARNING, e.getMessage());
			closeConnection();
			return false;
		}
		if (generatedKeys != null)
			try {
				if (generatedKeys.next()) {
					item.setId(generatedKeys.getInt(1));
					closeConnection();
					return true;
				}
				closeConnection();
				return false;
			} catch (SQLException e) {
				_logger.log(Level.WARNING, e.getMessage());
				closeConnection();
				return false;
			}
		closeConnection();
		return false;
	}

	/**
	 * method used by child classes to execute a SELECT in DB
	 * 
	 * @param statement String used to execute the exact query in DB (given by child
	 *                  classes)
	 * @param values    values to set in the query
	 * @return ResultSet necessary to construct the actual object get from DB
	 */
	protected static ResultSet get(String statement, String... values) {
		Connection connection = getConnection();
		try {
			PreparedStatement prepStat = connection.prepareStatement(statement);
			for (int i = 0; i < values.length; ++i)
				prepStat.setString(i + 1, values[i]);
			ResultSet resultSet = prepStat.executeQuery();
			if (resultSet.next())
				return resultSet;
			return null;
		} catch (SQLException e) {
			_logger.log(Level.WARNING, e.getMessage());
			return null;
		}
	}

	/**
	 * method used by child classes to addStep a new item in DB
	 * 
	 * @param item      the model item to be added (generic type)
	 * @param statement String used to execute the exact query in DB (given by child
	 *                  classes)
	 * @param values    values to set in the query
	 * @param           <T> the type of the added item
	 * @return boolean whether the addStep has been successful or not
	 */
	protected static <T extends AbstractItem> boolean add(T item, String statement, String... values) {
		PreparedStatement prepStat = add(statement, values);
		return prepStat != null && setGeneratedId(prepStat, item);
		// lazy evaluation to avoid setGeneratedId to get a null as prepStat
	}

	/**
	 * addStep an object to database
	 * 
	 * @param object the object added in DB
	 * @return boolean whether the insert has been successful or not
	 */
	@Override
	public abstract boolean add(T object);

	/**
	 * delete an object in database
	 * 
	 * @param object the object deleted in DB
	 * @return boolean whether the delete has been successful or not
	 */
	@Override
	public abstract boolean delete(T object);

	/**
	 * update an object in database
	 * 
	 * @param object the object updated in DB
	 * @return boolean whether the update has been successful or not
	 */
	@Override
	public abstract boolean update(T object);

	/**
	 * get an object from database
	 * 
	 * @param id the id of the object get from DB
	 * @return the object or null if none was found
	 */
	public abstract T get(int id);
}
