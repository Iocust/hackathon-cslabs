package app.controller.dao;

import static app.model.databaseUtils.AccountUtils.ADD;
import static app.model.databaseUtils.AccountUtils.DELETE;
import static app.model.databaseUtils.AccountUtils.GETBYID;
import static app.model.databaseUtils.AccountUtils.GETBYNAME;
import static app.model.databaseUtils.AccountUtils.UPDATE;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import app.model.Account;

/**
 * This class concretizes AbstractItemDAO. It focuses on accounts
 */
public class AccountDAO extends AbstractItemDAO<Account> {

	private static Logger _logger = Logger.getLogger("AccountDAO");

	/**
	 * addStep an account to database
	 * 
	 * @param account the account added in DB
	 * @return boolean whether the insert has been successful or not
	 */
	@Override
	public boolean add(Account account) {
		return add(account, ADD, account.getName(), account.getPassword());
	}

	/**
	 * delete an account in database
	 * 
	 * @param account the account deleted in DB
	 * @return boolean whether the delete has been successful or not
	 */
	@Override
	public boolean delete(Account account) {
		return delete(DELETE, account.getId());
	}

	/**
	 * update an account in database
	 * 
	 * @param account the account updated in DB
	 * @return boolean whether the update has been successful or not
	 */
	@Override
	public boolean update(Account account) {
		return update(UPDATE, account.getName(), account.getPassword(), Integer.toString(account.getId()));
	}

	/**
	 * get an account from database
	 * 
	 * @param id the id of the account get from DB
	 * @return the account or null if none was found
	 */
	@Override
	public Account get(int id) {
		ResultSet resultSet = get(GETBYID, Integer.toString(id));
		if (resultSet != null)
			try {
				Account res = new Account(id, resultSet.getString(1), resultSet.getString(2));
				closeConnection();
				return res;
			} catch (SQLException e) {
				_logger.log(Level.WARNING, e.getMessage());
			}
		closeConnection();
		return null;
	}

	/**
	 * get an account from database using username
	 * 
	 * @param username a String : the username of the account get from DB
	 * @return the account or null if none was found
	 */
	public Account get(String username) {
		ResultSet resultSet = get(GETBYNAME, username);
		if (resultSet != null)
			try {
				Account res = new Account(resultSet.getInt(1), username, resultSet.getString(2));
				closeConnection();
				return res;
			} catch (SQLException e) {
				_logger.log(Level.WARNING, e.getMessage());
			}
		closeConnection();
		return null;
	}
}
