package app.controller.dao;

import static app.model.databaseUtils.AnnonceUtils.ADD;
import static app.model.databaseUtils.AnnonceUtils.DELETE;
import static app.model.databaseUtils.AnnonceUtils.GETALL;
import static app.model.databaseUtils.AnnonceUtils.UPDATE;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import app.model.Annonce;

public class AnnonceDAO extends AbstractItemDAO<Annonce> {

	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static Logger _logger = Logger.getLogger("AnnonceDAO");

	@Override
	public boolean add(Annonce annonce) {
		return add(annonce, ADD, annonce.getDescription(), annonce.getName(), annonce.getRecompense(),
				annonce.getDebut().format(formatter), annonce.getFin().format(formatter),
				Double.toString(annonce.getLatitude()), Double.toString(annonce.getLongitude()),
				annonce.getEmplacement(), (annonce.getPosteur()));
	}

	@Override
	public boolean delete(Annonce annonce) {
		return delete(DELETE, annonce.getId());
	}

	@Override
	public boolean update(Annonce annonce) {
		return update(UPDATE, annonce.getName(), annonce.getDescription(), annonce.getRecompense(),
				(annonce.getPosteur()), annonce.getDebut().format(formatter), annonce.getFin().format(formatter),
				annonce.getEmplacement(), Double.toString(annonce.getLatitude()),
				Double.toString(annonce.getLongitude()), Integer.toString(annonce.getId()));
	}

	public ArrayList<Annonce> getAnnonces() {
		ResultSet resultSet = get(GETALL);
		if (resultSet != null)
			try {
				ArrayList<Annonce> res = new ArrayList<Annonce>();
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
				do {
					System.out.println("shit");
					res.add(new Annonce(Integer.valueOf(resultSet.getString(1)), 
							resultSet.getString(2),
							resultSet.getString(3), 
							resultSet.getString(4), 
							LocalDate.parse(resultSet.getString(5), formatter),
							LocalDate.parse(resultSet.getString(6), formatter), 
							Double.valueOf(resultSet.getString(7)),
							Double.valueOf(resultSet.getString(8)), 
							resultSet.getString(9), 
							resultSet.getString(10)));
				} while (resultSet.next());
				closeConnection();
				System.out.println(res.get(0));
				return res;
			} catch (SQLException e) {
				System.out.println("caca");
				_logger.log(Level.WARNING, e.getMessage());
			}
		closeConnection();
		return null;
	}

	@Override
	public Annonce get(int id) {
		// TODO Auto-generated by Kamen
		return null;
	}

}
