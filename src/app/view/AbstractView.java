package app.view;

import java.io.IOException;

import app.controller.AbstractViewHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * Abstract class containing methods necessary to all view subclasses.
 * 
 * @param <U>
 */
public abstract class AbstractView<T extends AbstractViewHandler<?>> {

	private Parent _root;
	private T _handler;
	private Pane _mainPane;
	private Stage _mainStage;
	private String _filename;

	public AbstractView(Pane pane, String filename) {
		_mainPane = pane;
		_filename = filename;
		loadFXML();
	}

	protected void setHandler(T handler) {
		_handler = handler;
	}

	public T getHandler() {
		return _handler;
	}

	public Stage getStage() {
		return _mainStage;
	}

	/**
	 * Loads the associated FXML file.
	 * 
	 * @return loaded file
	 */
	protected Parent loadFXML() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource(_filename));
		System.out.println(_filename);
		try {
			_root = loader.load();
			_handler = loader.getController();
		} catch (IOException except) {
			displayError("Error", "Failed to load the " + _filename + " file!");
			except.printStackTrace();
		}
		return _root;
	}

	/**
	 * Displays an error pop-up to the user.
	 * 
	 * @param header - The text to be displayed in the error's header.
	 * @param text   - The text to be displayed in the error's main field.
	 */
	public void displayError(String header, String text) {
		Alert error;
		error = new Alert(AlertType.ERROR);
		error.setTitle("Error");
		error.setHeaderText(header);
		error.setContentText(text);
		error.show();
	}

	/**
	 * Displays a message to the user using a pop-up.
	 * 
	 * @param header - The text to be displayed in the error's header.
	 * @param text   - The text to be displayed in the error's main field.
	 */
	public void displayMessage(String header, String text) {
		Alert alert;
		alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Information");
		alert.setHeaderText(header);
		alert.setContentText(text);
		alert.showAndWait();

	}

	/**
	 * Displays the window on the GUI.
	 */
	public void show() {
		if (_mainPane != null) {
			_mainPane.getChildren().add(_root);
			_mainStage = null;
		} else {
			_mainStage = new Stage();
			Scene scene = new Scene(_root);
			_mainStage.setScene(scene);
			_mainStage.show();
		}
	}
}
