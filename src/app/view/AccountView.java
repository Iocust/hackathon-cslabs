package app.view;

import app.controller.AccountViewHandler;
import javafx.scene.layout.Pane;

/**
 * Class launching the view used for logging in and registering. This is the
 * first view shown to the user.
 */
public class AccountView extends AbstractView<AccountViewHandler> {

	/**
	 * Used to instantiate the object and initialize variables.
	 */
	public AccountView(Pane pane) {
		super(pane, "/fxml/LoginGUI.fxml");
		getHandler().setView(this);
	}
}
