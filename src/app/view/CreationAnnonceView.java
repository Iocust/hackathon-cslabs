package app.view;

import app.controller.CreationAnnonceViewHandler;
import javafx.scene.layout.Pane;

/**
 * Class used to launch the application's main menu and window.
 */
public class CreationAnnonceView extends AbstractView<CreationAnnonceViewHandler> {

	/**
	 * Constructor - initializes necessary fields and performs first time
	 * operations.
	 */
	public CreationAnnonceView(Pane pane) {
		super(pane, "/fxml/CreationAnnonce.fxml");
		getHandler().setView(this);
	}
}