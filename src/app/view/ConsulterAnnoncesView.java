package app.view;

import app.controller.ConsulterAnnoncesViewHandler;
import javafx.scene.layout.Pane;

public class ConsulterAnnoncesView extends AbstractView<ConsulterAnnoncesViewHandler> {

	public ConsulterAnnoncesView(Pane pane) {
		super(pane, "/fxml/ConsulterAnnonces.fxml");
		getHandler().setView(this);
	}
}
