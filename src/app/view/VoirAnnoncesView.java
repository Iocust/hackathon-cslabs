package app.view;

import app.controller.VoirAnnoncesViewHandler;
import javafx.scene.layout.Pane;

public class VoirAnnoncesView extends AbstractView<VoirAnnoncesViewHandler> {

	/**
	 * Constructor - initializes necessary fields and performs first time
	 * operations.
	 */
	public VoirAnnoncesView(Pane pane) {
		super(pane, "/fxml/VoirAnnonce.fxml");
		getHandler().setView(this);
	}
}