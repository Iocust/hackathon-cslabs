package app.view;

import app.controller.MenuViewHandler;
import javafx.scene.layout.Pane;

/**
 * Class used to launch the application's main menu and window.
 */
public class MenuView extends AbstractView<MenuViewHandler> {

	/**
	 * Constructor - initializes necessary fields and performs first time
	 * operations.
	 */
	public MenuView(Pane pane) {
		super(pane, "/fxml/MenuGUI.fxml");
		getHandler().setView(this);
	}
}
