create table Account
(
	AccountId INTEGER not null
		primary key
		 autoincrement
		unique,
	Name TEXT not null
		unique,
	Password TEXT not null
)
;

create table Product
(
	ProductId INTEGER not null
		primary key
		 autoincrement
		unique,
	Name TEXT not null
		unique
)
;

create table ShoppingList
(
	ShoppingListId INTEGER not null
		primary key
	autoincrement
		unique,
	AccountId INTEGER not null
		references Account (AccountId),
	Name TEXT not null
)
;

create table Recipe
(
	RecipeId INTEGER not null
		primary key
	autoincrement
		unique,
	Persons INTEGER not null,
	AccountId INTEGER not null
		references Account (AccountId),
	Name TEXT not null
)
;

create table RecipeStep
(
  RecipeStepIndex INTEGER not null,
  Instructions TEXT not null,
  RecipeId INTEGER not null
    references Recipe (RecipeId)
)
;

create table Store
(
	StoreId INTEGER not null
		primary key
		 autoincrement
		unique,
	Name TEXT not null
		unique,
	Latitude DECIMAL(9,6) not null,
	Longitude DECIMAL(9,6) not null,
	Address TEXT not null
)
;

create table hasProductInShoppingList
(
	ProductId INTEGER not null
		references Product (ProductId),
	ShoppingListId INTEGER not null
		references ShoppingList (ShoppingListId),
	Quantity INTEGER not null,
	primary key (ShoppingListId, ProductId)
)
;

create table hasProductInRecipe
(
	ProductId INTEGER not null
		references Product (ProductId),
	RecipeId INTEGER not null
		references ShoppingList (ShoppingListId),
	Quantity INTEGER not null,
	primary key (RecipeId, ProductId)
)
;

create table hasProductInStore
(
	ProductId INTEGER not null
		references Product (ProductId),
	StoreId INTEGER not null
		references Store (StoreId),
	QuantityPerProduct FLOAT not null,
	Description TEXT not null,
	Price DECIMAL(6,2) not null,
	primary key (ProductId, StoreId)
)
;

CREATE TABLE Annonces(
	id_annonce INTEGER PRIMARY KEY autoincrement,
	description TEXT NOT NULL,
	name TEXT NOT NULL,
    recompense TEXT NOT NULL,
    debut TEXT NOT NULL,
    fin TEXT NOT NULL,
    longitude TEXT NOT NULL,
    latitude TEXT NOT NULL,
    emplacement TEXT NOT NULL,
	id_creator INTEGER REFERENCES Account (AccountId) NOT NULL
);



CREATE TABLE Users_Annonces(
	user_id INTEGER references Account(AccountId) NOT NULL,
	annonce_id INTEGER references Annonces(id_annonce) NOT NULL,
	PRIMARY KEY(user_id,annonce_id) 


);